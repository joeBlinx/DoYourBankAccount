package com.example.unnom.doyourbankaccount;

import android.text.TextUtils;
import android.widget.EditText;

/**
 * Created by unnom on 31/10/16.
 */
public class Error {

    public static boolean emptyField(EditText text){
        String test = text.getText().toString();
        if(TextUtils.isEmpty(test)){
            text.setError("Le champ est vide");
            return true;
        }
        return false;
    }

    public static boolean isNotValide(EditText text){
        String strTest = text.getText().toString();
        boolean test = false;
        if(strTest.substring(0, 1).equals(".") || strTest.substring(strTest.length()-1).equals(".")){
            test = true;
        }

        if(test){
            text.setError("Nombre non valide");
        }


        return test;
    }
}
