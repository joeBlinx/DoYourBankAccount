package com.example.unnom.doyourbankaccount;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Scanner;

/**
 * Created by unnom on 30/10/16.
 */
public class File extends AppCompatActivity {
    private LinearLayout linearLayout;
    private Array<TextView> texts;
    private String path;

    protected void onCreate(Bundle save) {
        super.onCreate(save);
        Bundle pathOfFile = getIntent().getExtras();
        path = pathOfFile.getString("path");
        linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        texts = new Array<>();
        setContentView(linearLayout);
        init(path);
    }

    /**
     * @brief : Initialise l'affichage du fichier, avec calcul en temps réel du montant restant
     * @param path : chemin du fichier a traiter
     */


    private void init(String path) {
        try {
            FileInputStream file = openFileInput(path);
            Scanner scanner = new Scanner(file);
            scanner.useDelimiter(";");
            String read;
            float soldes = 0;
            int i = 0;
            while (scanner.hasNext()) {
                String temp = scanner.next();
                texts.add(new TextView(this));
                texts.getLast().setText(temp);
                texts.getLast().setTextSize(15);
                linearLayout.addView(texts.getLast());
                if ((i % 2) == 1) {
                    float add = Float.parseFloat(temp);
                    if (i > 2) {
                        soldes -= add;
                        texts.getLast().setText(texts.getLast().getText() + " euros");
                    } else
                        soldes += add;
                }else{
                    texts.getLast().setTypeface(Typeface.DEFAULT_BOLD);
                }
                i++;

            }
            soldes = (soldes*100);
            int tmp = (int)soldes;
            soldes = tmp/100.0f;
            texts.get(1).setText(Float.toString(soldes) + " euros");
            for(int j = 0 ; j < 2; j++) {
                texts.get(j).setGravity( Gravity.CENTER_HORIZONTAL);
                texts.get(j).setTypeface(Typeface.DEFAULT_BOLD);
                texts.get(j).setTextSize(20);
            }
            if(soldes >= 0){
                texts.get(1).setTextColor(0xFF00A000);
            }else{
                texts.get(1).setTextColor(Color.RED);
            }


        } catch (IOException e) {
            Toast toast = Toast.makeText(this, path + "n'existe pas", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    protected void onResume() {
        super.onResume();
        texts.clear();
        linearLayout.removeAllViews();
        init(path);

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_file, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.addDepense:
                Intent intent = new Intent(this, Depense.class);
                intent.putExtra("path", path);
                startActivity(intent);
                return true;
            case R.id.delete:
                deleteFile(path);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

