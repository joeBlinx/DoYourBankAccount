package com.example.unnom.doyourbankaccount;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.*;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;


public class MainActivity extends AppCompatActivity {
    private ManageFile manage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        manage = new ManageFile(this);
        registerForContextMenu (manage.getList());
        setContentView(manage.getList());


    }

    @Override
    protected void onResume() {
        super.onResume();
        manage.initFile();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    public boolean onOptionsItemSelected (MenuItem item ){
        if(item.getItemId() == R.id.add) {
           /* Toast toast = Toast.makeText(this, "adsffd", Toast.LENGTH_SHORT);
            toast.show();*/
            Intent intent = new Intent(this, FileCreation.class);
            startActivity(intent);
            return true;
        }
       return super.onOptionsItemSelected(item);
    }
    public void onCreateContextMenu (ContextMenu menu, View vue, ContextMenu.ContextMenuInfo menuInfo){
        super.onCreateContextMenu(menu, vue, menuInfo);
        menu.add("Supprimer");


    }
    @Override

    public boolean onContextItemSelected(MenuItem item) {






        return super.onContextItemSelected(item);

    }



}